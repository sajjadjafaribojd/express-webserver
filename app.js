/* jshint esversion:6 */

const express=require('express');
const hbs=require('hbs');
const fs=require('fs');

const port=process.env.PORT || 3000;

var app=express();

app.set('view engine','hbs');// view engine. Using hbs as the default view engine requires 
//To use a different extension (i.e. html) for your template files:

/*
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
*/

hbs.registerPartials(__dirname + '/views/partials');
hbs.registerHelper('getCurrentYear',()=>{
    return new Date().getFullYear(); //show in footer.hbs
});
hbs.registerHelper('screamIt',(text)=>{ //show in home.hbs firts function
    return text.toUpperCase();
});



app.use((req,res,next)=>{
    var now=new Date();
    var log=`${now}:${req.method}:${req.url} `;

    console.log(log);
    fs.appendFile('server.log',log+'\n',(err)=>{
        if(err){
            console.log('Write log failed');
        }
    });
    next();
});

/*app.use((req,res,next)=>{
    res.render('maintenance');
});*/

app.use(express.static(__dirname+'/public')); //localhost:3000/help.html
app.use(express.static(__dirname+'/private'));//localhost:3000/login.html

app.get('/',(req,res)=>{

    res.render('home.hbs',{
        title:'Home page',
        content:'Sepcial home page carete with JabJ',
        //currentYear: new  Date().getFullYear() ; //user hbs helper
    });

    //res.send('Hello world!');
   /* res.send({
        some:'json',
        name:'sajjad'
    });
    res.send('<h1>html tag</h1');
    res.status(404).send('Sorry can not find that');*/
});

app.get('/about',(req,res)=>{
    //res.render('about.hbs');

    res.render('about.hbs',{
        tilte:'About',
        headerText:'About JabJ text',
       // currentYear: new Date().getFullYear() //use hbs helper
    });
});


app.listen(port,'localhost',()=>{
    console.log(`Express runing on prot:${port}`);
});
 